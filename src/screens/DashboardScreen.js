import React, { Component } from 'react';
import { View, StyleSheet, Dimensions } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Icon } from 'react-native-elements';
import { DeckSwiper, Text } from 'native-base';
import { connect } from 'react-redux';
import firebase from 'firebase';

import { Spinner } from '../components/common';
import SlipperyCard from '../components/SlipperyCard';

class DashboardScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cards: null,
      loading: true
    };
  }

  componentDidMount() {
    firebase
      .database()
      .ref()
      .child('/users/')
      .once('value')
      .then(
        queryResult => {
          this.setState({
            cards: Object.keys(queryResult.val())
              .filter(userId => {
                if (userId === this.props.user) {
                  return false;
                } else if (
                  Array.isArray(queryResult.val()[this.props.user].likes)
                ) {
                  return !queryResult
                    .val()
                    [this.props.user].likes.includes(userId);
                }
                return true;
              })
              .map(userId => {
                const user = queryResult.val()[userId];
                const userImage = (user.pictures && user.pictures.length > 0) ?
                                  (user.pictures[0]) :
                                  ('https://firebasestorage.googleapis.com/v0' +
                                   '/b/dating-mobile-app.appspot.com/o/assets' +
                                   '%2Fno-image-icon-23499.png?alt=media&toke' +
                                   'n=d44ff94e-3313-4380-9343-ad946c1965b2');
                return {
                  id: userId,
                  first_name: user.personal_informations.first_name,
                  age: user.personal_informations.age,
                  image: userImage
                };
              }),
            loading: false
          });
        },
        error => {
          console.log('error while getting data');
          console.log(error);
          this.setState({ cards: null, loading: true });
        }
      )
      .catch(error => {
        console.log(error);
      });
  }

  likeUser = user => {
    console.log('I like user:', user);
    const likesRef = firebase
      .database()
      .ref()
      .child(`/users/${this.props.user}/likes/`);
    likesRef
      .once('value')
      .then(
        likes => {
          console.log(likes);
          const likeList = Object.values(likes.val()) || [];
          if (likeList.includes(user.id)) {
            console.log(
              'User ',
              user.id,
              ' is already in the list. Some error might happened'
            );
          } else {
            likeList.push(user.id);
            likesRef
              .set(likeList)
              .then(
                result => {
                  console.log('Like was successfully added: ', result);
                },
                error => {
                  console.log('Error while adding new like: ', error);
                }
              )
              .catch(error => {
                console.log('Error while adding new like: ', error);
              });
            console.log(likes.val());
          }
        },
        error => {
          console.log('Error while getting likes: ', error);
        }
      )
      .catch(error => {
        console.log('Error while getting likes: ', error);
      });
  };

  dislikeUser = user => {
    console.log('I dislike user:', user);
  };

  renderEmpty = () => (
    <View style={{ alignSelf: 'center' }}>
      <Text>Over</Text>
    </View>
  );

  render() {
    let dashBoardView;

    if (this.state.loading && !this.state.cards) {
      dashBoardView = <Spinner size="large" />;
    } else {
      dashBoardView = (
        <View style={styles.container}>
          <View style={styles.cardContainer}>
            <DeckSwiper
              ref={c => (this._deckSwiper = c)}
              dataSource={this.state.cards}
              renderEmpty={() => this.renderEmpty()}
              renderItem={cardData => <SlipperyCard {...cardData} />}
              onSwipeRight={user => this.likeUser(user)}
              onSwipeLeft={user => this.dislikeUser(user)}
            />
          </View>

          <View style={styles.buttonContainer}>
            <Icon
              raised
              name="thumbs-down"
              type="font-awesome"
              color="#1a5599"
              reverse
              onPress={() => {
                this._deckSwiper._root.swipeLeft();
                this.dislikeUser(this._deckSwiper._root.state.selectedItem);
              }}
            />
            <Icon
              raised
              name="sliders"
              type="font-awesome"
              color="#88898c"
              reverse
              onPress={() => Actions.partnersearchsettings()}
            />
            <Icon
              raised
              name="heart"
              type="font-awesome"
              color="#f81b84"
              reverse
              onPress={() => {
                this._deckSwiper._root.swipeRight();
                this.likeUser(this._deckSwiper._root.state.selectedItem);
              }}
            />
          </View>
        </View>
      );
    }
    return dashBoardView;
  }
}

const MapStateToProps = ({ auth }) => {
  const { user } = auth;
  return { user };
};

export default connect(
  MapStateToProps,
  {}
)(DashboardScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    alignContent: 'space-between',
    justifyContent: 'space-between'
  },
  cardContainer: {
    flex: 1,
    marginTop: 20
  },
  buttonContainer: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-around'
  }
});
