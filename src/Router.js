import React from 'react';
import { StyleSheet } from 'react-native';
import { Scene, Router, Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/FontAwesome';

import Background from './components/Background';
import FAQScreen from './screens/settings/FAQScreen';
import ChatScreen from './screens/ChatScreen';
import LoginScreen from './screens/authentication/LoginScreen';
import SignUpScreen from './screens/authentication/SignUpScreen';
import SettingsScreen from './screens/settings/SettingsScreen';
import MessagesScreen from './screens/MessagesScreen';
import DashboardScreen from './screens/DashboardScreen';
import LoginWithEmailScreen from './screens/authentication/LoginWithEmailScreen';
import AccountSettingsScreen from './screens/settings/AccountSettingsScreen';
import PersonalInformationScreen from './screens/settings/PersonalInformationScreen';
import DetailedInformationScreen from './screens/DetailedInformationScreen';
import PartnerSearchSettingsScreen from './screens/PartnerSearchSettingsScreen';

const sceneConfig = {
  cardStyle: { backgroundColor: 'transparent', fontSize: 50 }
};

const RouterComponent = () => (
  <Background>
    <Router
      {...sceneConfig}
      tintColor="#666"
      titleStyle={styles.title}
      navigationBarStyle={styles.navigationBar}
      component={Background}
    >
      <Scene key="root" hideNavBar>
        <Scene key="auth" initial>
          <Scene key="login" component={LoginScreen} title="Log in" />
          <Scene
            key="signupwithemail"
            component={SignUpScreen}
            title="Sign up"
          />

          <Scene
            key="loginwithemail"
            title="Log in with Email"
            component={LoginWithEmailScreen}
          />
        </Scene>

        <Scene key="main">
          <Scene
            initial
            key="dashboard"
            title="Dashboard"
            component={DashboardScreen}
            rightTitle={
              <Icon name="commenting" style={styles.icon} size={29} />
            }
            leftTitle={<Icon name="gear" style={styles.icon} size={29} />}
            onRight={() => {
              Actions.messages();
            }}
            onLeft={() => {
              Actions.settings();
            }}
          />

          <Scene
            key="detailedinformation"
            title="Information"
            component={DetailedInformationScreen}
          />

          <Scene
            key="partnersearchsettings"
            title="Partner Search Parameters"
            component={PartnerSearchSettingsScreen}
          />

          <Scene key="messages" title="Messages" component={MessagesScreen} />

          <Scene key="chat" title="Chat" component={ChatScreen} />

          <Scene key="settings" title="Settings" component={SettingsScreen} />

          <Scene
            key="personalinformation"
            title="Personal Information"
            component={PersonalInformationScreen}
          />

          <Scene
            key="accountsettings"
            title="Account Settings"
            component={AccountSettingsScreen}
          />

          <Scene key="faq" title="FAQ" component={FAQScreen} />
        </Scene>
      </Scene>
    </Router>
  </Background>
);

const styles = StyleSheet.create({
  navigationBar: {
    backgroundColor: '#fff'
  },
  title: {
    flex: 1,
    alignSelf: 'center',
    textAlign: 'center',
    color: '#666',
    fontSize: 22
  },
  icon: {
    color: '#666'
  }
});

export default RouterComponent;
