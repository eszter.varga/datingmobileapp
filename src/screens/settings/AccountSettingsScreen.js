import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import { List, ListItem, Button } from 'react-native-elements';
import { myAccountInformation } from '../../assets/data';

const list = [
  {
    title: 'D0L0T0',
    icon: 'av-timer',
    inner: [
      {
        title: 'D1L0T0',
        icon: 'av-timer',
        value: 'Sajt'
      },
      {
        title: 'D1L1T0',
        icon: 'flight-takeoff',
        inner: [
          {
            title: 'D2L0T0',
            icon: 'av-timer',
            inner: [
              {
                title: 'D3L0T0',
                icon: 'av-timer',
                value: false
              },
              {
                title: 'D3L1T0',
                icon: 'av-timer',
                value: 3
              }
            ]
          },
          {
            title: 'D2L1T0',
            icon: 'av-timer',
            inner: [
              {
                title: 'D3L0T1',
                icon: 'av-timer',
                value: 'aaa'
              },
              {
                title: 'D3L1T1',
                icon: 'av-timer',
                value: true
              }
            ]
          }
        ]
      }
    ]
  },
  {
    title: 'D0L1T0',
    icon: 'flight-takeoff',
    value: 0
  }
];

class AccountSettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      self: myAccountInformation,
      currentSubList: list,
      parent: []
    };
  }

  onElementPress = inner => {
    if (inner) {
      const parentObj = this.state.parent;
      parentObj.push(this.state.currentSubList);

      this.setState({
        parent: parentObj,
        currentSubList: inner
      });
    }
  };

  onBackPress = () => {
    if (this.state.parent.length > 0) {
      this.setState({
        currentSubList: this.state.parent.pop()
      });
    }
  };

  renderTypeDependentListItem = item => {
    switch (typeof item.value) {
      case 'string':
        return (
          <ListItem
            textInputMultiline
            hideChevron
            key={item.title}
            title={item.title}
            leftIcon={{ name: item.icon }}
          />
        );
      case 'number':
        return (
          <ListItem
            textInput
            textInputKeyboardType="numbers-and-punctuation"
            hideChevron
            key={item.title}
            title={item.title}
            leftIcon={{ name: item.icon }}
          />
        );
      case 'boolean':
        return (
          <ListItem
            switchButton
            hideChevron
            key={item.title}
            title={item.title}
            leftIcon={{ name: item.icon }}
          />
        );
      default:
        return (
          <ListItem
            key={item.title}
            title={item.title}
            leftIcon={{ name: item.icon }}
            onPress={() => {
              this.onElementPress(item.inner);
            }}
          />
        );
    }
  };

  render() {
    return (
      <ScrollView>
        <Button
          title="Back"
          disabled={this.state.parent.length < 1}
          onPress={this.onBackPress}
        />
        <List>
          {this.state.currentSubList.map(item =>
            this.renderTypeDependentListItem(item)
          )}
        </List>
      </ScrollView>
    );
  }
}

export default AccountSettingsScreen;
