import React, { Component } from 'react';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import firebase from 'firebase';
import { createStore, applyMiddleware } from 'redux';
import { GoogleSignin } from 'react-native-google-signin';

import Router from './Router';
import reducers from './reducers';

class App extends Component {
  componentWillMount() {
    // Initialize Firebase
    const config = {
      apiKey: 'AIzaSyAprOR0AkSP95XasUyIamrSgyFwLzTzGUY',
      authDomain: 'dating-mobile-app.firebaseapp.com',
      databaseURL: 'https://dating-mobile-app.firebaseio.com',
      projectId: 'dating-mobile-app',
      storageBucket: 'dating-mobile-app.appspot.com',
      messagingSenderId: '640520710588'
    };
    firebase.initializeApp(config);

    GoogleSignin.configure({
      scopes: ['https://www.googleapis.com/auth/drive.readonly'],
      webClientId:
        '640520710588-senarq8m7l5kfdv9t7h7mvq6jcg74r44.apps.googleusercontent.com'
    });
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

    const Data = firebase.database();
    console.log('firebase data: ', Data);

    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

export default App;
