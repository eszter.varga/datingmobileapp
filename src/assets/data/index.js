export const myAccountInformation = {
  email: 'elisasmith@gmail.com',
  password: '*******',
  showDistance: true,
  showOnlineStatus: true,
  notifications: {
    email: true,
    push_notification: true
  }
};

export const myPersonalInformation = {
  name: 'Elisa Smith',
  birthday: '1991.01.01.',
  sex: ['Female'],
  about_me:
    'Lorem ipsum dolor sit amet, veniam utamur ut sed, et pri solum modus.',
  languages: ['Hungarian', 'English'],
  living_circumtances: ['Living with my partner'],
  sexual_orientation: ['Heterosexual'],
  relationship: ['In a relationship'],
  children: 0,
  height: 171,
  weight: 77,
  physique: ['Average'],
  hair_color: ['Brown'],
  eye_color: ['Brown'],
  smoking: false,
  alcohol: true
};

export const mySearchSettings = {
  sex: ['Male', 'Female'],
  minAge: 0,
  maxAge: 100,
  maximalDistance: 100,
  languages: ['Hungarian'],
  living_circumtances: [
    'Living with my partner',
    'Living Alone',
    'Living with my parents',
    'Living with roommates',
    'Living in a collage'
  ],
  sexual_orientation: ['Heterosexual', 'Homosexual', 'Bisexual'],
  relationship: ['In a relationship', 'Single', 'Complicated'],
  minChildren: 0,
  maxChildren: 3,
  minHeight: 171,
  maxHeight: 190,
  minWeight: 77,
  maxWeight: 130,
  physique: ['Average', 'Fat', 'Thin'],
  hair_color: ['Bald', 'Brown', 'Black', 'Grey', 'Blonde', 'Red', 'Other'],
  eye_color: ['Brown', 'Green', 'Blue', 'Black', 'Grey', 'Other'],
  smoking: false,
  alcohol: true
};

export const cards = [
  {
    id: 1,
    first_name: 'Elisabeth Thomasina Amanda',
    age: 21,
    friends: 9,
    interests: 38,
    image: require('../images/image1.jpeg')
  },
  {
    id: 2,
    first_name: 'Cynthia',
    age: 27,
    friends: 16,
    interests: 49,
    image: require('../images/image2.jpeg')
  },
  {
    id: 3,
    first_name: 'Maria',
    age: 29,
    friends: 2,
    interests: 39,
    image: require('../images/image3.jpeg')
  },
  {
    id: 4,
    first_name: 'Jessica',
    age: 20,
    friends: 18,
    interests: 50,
    image: require('../images/image4.jpeg')
  },
  {
    id: 5,
    first_name: 'Julie',
    age: 28,
    friends: 2,
    interests: 13,
    image: require('../images/image5.jpeg')
  },
  {
    id: 6,
    first_name: 'Anna',
    age: 24,
    friends: 12,
    interests: 44,
    image: require('../images/image6.jpeg')
  }
];

export const chatRows = [
  {
    id: 1,
    name: 'Diane',
    message: 'Suspendisse accumsan tortor quis turpis.',
    image: require('../images/image1.jpeg')
  },
  {
    id: 2,
    name: 'Lois',
    message:
      'Morbi sem mauris, laoreet ut, rhoncus aliquet, pulvinar sed, nisl.',
    image: require('../images/image2.jpeg')
  },
  {
    id: 3,
    name: 'Mary',
    message: 'Duis bibendum.',
    image: require('../images/image3.jpeg')
  },
  {
    id: 4,
    name: 'Susan',
    message: 'Praesent blandit.',
    image: require('../images/image4.jpeg')
  },
  {
    id: 5,
    name: 'Betty',
    message: 'Mauris enim leo, rhoncus sed, vestibulum, cursus id, turpis.',
    image: require('../images/image5.jpeg')
  },
  {
    id: 6,
    name: 'Deborah',
    message: 'Aliquam sit amet diam in magna bibendum imperdiet.',
    image: require('../images/image6.jpeg')
  },
  {
    id: 7,
    name: 'Frances',
    message: 'Phasellus sit amet erat.',
    image: require('../images/image7.jpeg')
  },
  {
    id: 8,
    name: 'Joan',
    message: 'Vestibulum ante ipsum bilia Curae; Duis faucibus accumsan odio.',
    image: require('../images/image8.jpeg')
  },
  {
    id: 9,
    name: 'Denise',
    message: 'Aliquam non mauris.',
    image: require('../images/image9.jpeg')
  },
  {
    id: 10,
    name: 'Rachel',
    message: 'Nulla ac enim.',
    image: require('../images/image10.jpeg')
  }
];

export const newMatches = [
  {
    id: 1,
    first_name: 'Sarah',
    image: require('../images/image7.jpeg')
  },
  {
    id: 2,
    first_name: 'Pamela',
    image: require('../images/image8.jpeg')
  },
  {
    id: 3,
    first_name: 'Diana',
    image: require('../images/image9.jpeg')
  },
  {
    id: 4,
    first_name: 'Christina',
    image: require('../images/image10.jpeg')
  },
  {
    id: 5,
    first_name: 'Rebecca',
    image: require('../images/image11.jpeg')
  },
  {
    id: 6,
    first_name: 'Wanda',
    image: require('../images/image5.jpeg')
  },
  {
    id: 7,
    first_name: 'Sara',
    image: require('../images/image6.jpeg')
  },
  {
    id: 8,
    first_name: 'Judith',
    image: require('../images/image7.jpeg')
  },
  {
    id: 9,
    first_name: 'Ruby',
    image: require('../images/image1.jpeg')
  },
  {
    id: 10,
    first_name: 'Sandra',
    image: require('../images/image11.jpeg')
  }
];
