import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';

class DetailedInformationScreen extends Component {
  state = {};
  render() {
    return (
      <View style={styles.container}>
        <Text>Detailed Information</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  }
});

export default DetailedInformationScreen;
