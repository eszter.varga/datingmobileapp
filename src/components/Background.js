import React, { Component } from 'react';
import { View, StyleSheet, ImageBackground } from 'react-native';

class Background extends Component {
  render() {
    return (
      <ImageBackground
        style={styles.container}
        source={require('../assets/images/hearts5.jpg')}
      >
        <View style={styles.overlay}>{this.props.children}</View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  overlay: {
    flex: 1,
    backgroundColor: 'rgba(255,255,255,0.8)',
    flexDirection: 'column',
    alignContent: 'space-between',
    justifyContent: 'space-between'
  }
});

export default Background;
