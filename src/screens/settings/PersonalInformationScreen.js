import React, { Component } from 'react';
import { ScrollView, Text } from 'react-native';
import { Card } from 'react-native-elements';
import { myPersonalInformation } from '../../assets/data';

class PersonalInformationScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      self: myPersonalInformation
    };
  }
  render() {
    const { self } = this.state;
    return (
      <ScrollView>
        <Card>
          <Text>Name: {self.name}</Text>
        </Card>
        <Card>
          <Text>Birthday: {self.birthday}</Text>
        </Card>
        <Card>
          <Text>Sex: {self.sex}</Text>
        </Card>
        <Card>
          <Text>About me: {self.about_me}</Text>
        </Card>
        <Card>
          <Text>Languages: {self.languages.join(' ')}</Text>
        </Card>
        <Card>
          <Text>Living circumstances: {self.living_circumtances}</Text>
        </Card>
        <Card>
          <Text>Sexual orientation: {self.sexual_orientation}</Text>
        </Card>
        <Card>
          <Text>Relationship: {self.relationship}</Text>
        </Card>
        <Card>
          <Text>Children: {self.children}</Text>
        </Card>
        <Card>
          <Text>Height: {self.height} cm</Text>
        </Card>
        <Card>
          <Text>Weight: {self.weight} kg</Text>
        </Card>
        <Card>
          <Text>Physique: {self.physique}</Text>
        </Card>
        <Card>
          <Text>Hair color: {self.hair_color}</Text>
        </Card>
        <Card>
          <Text>Eye color: {self.eye_color}</Text>
        </Card>
        <Card>
          <Text>Smoking: {self.smoking ? 'Yes' : 'No'}</Text>
        </Card>
        <Card>
          <Text>Alcohol: {self.alcohol ? 'Yes' : 'No'}</Text>
        </Card>
      </ScrollView>
    );
  }
}

export default PersonalInformationScreen;
