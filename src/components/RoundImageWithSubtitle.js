import React from 'react';
import { Image, Text, StyleSheet, TouchableOpacity } from 'react-native';

const RoundImageWithSubtitle = (image, name, onPress) => (
  <TouchableOpacity
    onPress={() => onPress()}
    style={styles.roundImageContainer}
  >
    <Image source={image} style={styles.roundImage} />
    <Text style={styles.roundImageSubTitle}>{name}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  roundImageContainer: {
    alignItems: 'center'
  },
  roundImage: {
    width: 70,
    height: 70,
    borderRadius: 35,
    margin: 10
  },
  roundImageSubTitle: {
    fontWeight: '600',
    color: '#444'
  }
});

export default RoundImageWithSubtitle;
