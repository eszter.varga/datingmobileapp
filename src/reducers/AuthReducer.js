import {
  SET_ERROR,
  CLEAR_ERROR,
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGOUT_USER,
  REGISTER_USER,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAIL
} from '../actions/types';

const INITIAL_STATE = {
  user: null,
  error: '',
  loading: false,
  loginType: null,
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case SET_ERROR:
      return { ...state, error: action.payload };
    case CLEAR_ERROR:
      return { ...state, error: null };
    case LOGIN_USER:
      return { ...state, loginType: action.loginType, loading: true, error: null };
    case LOGIN_USER_SUCCESS:
      return { ...state, user: action.user, loading: false, error: null };
    case LOGIN_USER_FAIL:
      return { ...state, user: null, loginType: null, loading: false, error: action.error };
    case LOGOUT_USER:
      return { ...state, user: null, loginType: null };
    case REGISTER_USER:
      return { ...state, ...INITIAL_STATE, loginType: action.loginType, loading: true };
    case REGISTER_USER_SUCCESS:
      return { ...state, ...INITIAL_STATE, error: null, loading: false };
    case REGISTER_USER_FAIL:
      return { ...state, ...INITIAL_STATE, error: action.error, loading: false };
    default:
      return state;
  }
};
