import React, { Component } from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from 'react-native';
import { Icon } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';

class SlipperyCard extends Component {
  render() {
    return (
      <View style={styles.card}>
        <Image style={styles.thumbnail} source={{ uri: this.props.image }} />
        <View style={styles.textContainer}>
          <Text style={styles.text} numberOfLines={1} ellipsizeMode="tail">
            {this.props.first_name}, {this.props.age}
          </Text>
          <Icon
            raised
            reverse
            name="account-card-details"
            type="material-community"
            color="rgba(255, 255, 255, 0.2)"
            onPress={() => {
              Actions.detailedinformation();
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    height: Dimensions.get('window').height * 0.7,
    width: Dimensions.get('window').height * 0.55,
    elevation: 10,
    borderWidth: 0,
    borderRadius: 35,
    borderColor: 'black',
    alignSelf: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
    shadowRadius: 10,
    shadowOpacity: 0.9,
    shadowColor: 'black',
    shadowOffset: { width: 3, height: 3 }
  },
  thumbnail: {
    flex: 1,
    margin: 0,
    height: '100%',
    width: '100%',
    borderRadius: 30,
    resizeMode: 'cover'
  },
  text: {
    color: 'white',
    fontSize: 35,
    paddingTop: 5,
    paddingBottom: 5,
    width: 'auto',
    maxWidth: '70%',
    fontWeight: 'bold',
    textShadowRadius: 20,
    textShadowColor: 'black',
    textShadowOffset: { width: 3, height: 3 }
  },
  textContainer: {
    flex: 2,
    top: 0,
    left: 30,
    right: 20,
    bottom: 20,
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'space-between'
  }
});

export default SlipperyCard;
