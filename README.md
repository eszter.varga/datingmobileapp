# DatingApp

## How to

### Install on linux

```
# Checkout the repo
git clone https://gitlab.com/eszter.toth/datingmobileapp.git

# Make sure you have npm installed
sudo apt-get install npm

# Make sure you have react-native-cli installed
sudo npm install -g react-native-cli

# Go into the app folder
cd datingmobileapp

# Install dependencies
npm install

# Add proper rights to gradle
chmod 755 android/gradlew

# Set sdk path property
# The default SDK path is /home/$USER/Android/Sdk
echo "sdk.dir = $ANDROID_HOME" > android/local.properties

# Start the android emulator by android-studio or cli

# Run the app
react-native run-android
```
