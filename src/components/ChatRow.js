import React from 'react';
import { View, Image, Text, StyleSheet, TouchableOpacity } from 'react-native';

const ChatRow = (image, name, message, onPress) => (
  <TouchableOpacity onPress={() => onPress()} style={styles.chatRow}>
    <Image source={image} style={styles.chatImage} />
    <View>
      <Text style={styles.nameText}>{name}</Text>
      <Text numberOfLines={1} style={styles.messageSubstringText}>
        {message}
      </Text>
    </View>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  chatRow: {
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 5,
    marginBottom: 5,
    borderBottomWidth: 1,
    borderColor: '#e3e3e3'
  },
  chatImage: {
    width: 70,
    height: 70,
    borderRadius: 35,
    margin: 10
  },
  nameText: {
    fontWeight: '600',
    color: '#111'
  },
  messageSubstringText: {
    fontWeight: '400',
    color: '#888',
    width: 200
  }
});

export default ChatRow;
