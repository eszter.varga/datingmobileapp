import { AppRegistry, YellowBox } from 'react-native';
import App from './src/App';

//temporary solution for Timer warnings,
//need to test react-native-firebase later to solve this issue
YellowBox.ignoreWarnings(['Setting a timer']);

AppRegistry.registerComponent('DatingMobileApp', () => App);
