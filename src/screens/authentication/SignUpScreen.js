import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import {
  Card,
  CardSection,
  Input,
  Button,
  Spinner
} from '../../components/common';
import { registerUser, setError, clearError } from '../../actions/auth';

class SignUpScreen extends Component {

  constructor(props) {
    super(props);

    this.state = {
      email: '',
      password: '',
      rePassword: '',
    };
  }

  componentWillMount() {
    this.props.clearError();
  }

  onEmailChange = text => {
    this.setState({
      email: text
    });
  }

  onPasswordChange = text => {
    this.setState({
      password: text
    });
  }

  onRePasswordChange = text => {
    this.setState({
      rePassword: text
    });
  }

  onButtonPress = () => {
    if (!this.state.password || !this.state.email) {
      this.props.setError('Email or password field is empty!');
      return;
    }

    if (this.state.password !== this.state.rePassword) {
      this.props.setError('Passwords do not match!');
      return;
    }

    this.props.registerUser('email', this.state.email, this.state.password);
  };

  renderError = () => {
    if (this.props.error) {
      return (
        <View style={{ backgroundColor: 'white' }}>
          <Text style={styles.errorTextStyle}>{this.props.error}</Text>
        </View>
      );
    }
  };

  renderButton = () => {
    if (this.props.loading) {
      return <Spinner size='large' />;
    }

    return <Button onPress={this.onButtonPress}>Sign up</Button>;
  };

  render() {
    return (
      <Card>
        <CardSection>
          <Input
            label='Email'
            placeholder='email@gmail.com'
            onChangeText={this.onEmailChange}
            value={this.state.email}
            keyboardType="email-address"
            textContentType="emailAddress"
          />
        </CardSection>
        <CardSection>
          <Input
            secureTextEntry
            label='Password'
            placeholder='password'
            onChangeText={this.onPasswordChange}
            value={this.state.password}
          />
        </CardSection>
        <CardSection>
          <Input
            secureTextEntry
            label='Password again'
            placeholder='password again'
            onChangeText={this.onRePasswordChange}
            value={this.state.rePassword}
          />
        </CardSection>
        {this.renderError()}
        <CardSection>{this.renderButton()}</CardSection>
      </Card>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};

const MapStateToProps = ({ auth }) => {
  const { error, loading } = auth;
  return { error, loading };
};

export default connect(
  MapStateToProps,
  { registerUser, clearError, setError }
)(SignUpScreen);
