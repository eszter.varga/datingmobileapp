import { Actions } from 'react-native-router-flux';
import firebase from 'firebase';
import { GoogleSignin, statusCodes } from 'react-native-google-signin';
import { LoginManager, AccessToken } from 'react-native-fbsdk';
import {
  SET_ERROR,
  CLEAR_ERROR,
  LOGIN_USER,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGOUT_USER,
  REGISTER_USER,
  REGISTER_USER_SUCCESS,
  REGISTER_USER_FAIL
} from './types';

export const setError = errorMsg => ({
  type: SET_ERROR,
  payload: errorMsg
});

export const clearError = () => ({
  type: CLEAR_ERROR
});

export const loginUserDefault = loginType => dispatch => {
  dispatch({ type: LOGIN_USER, loginType });
  return firebase.auth().onAuthStateChanged(user => {
    if (user) {
      console.log('User was logged in by default', user);
      dispatch({
        type: LOGIN_USER_SUCCESS,
        user: user.uid
      });
      Actions.main();
    } else {
      dispatch({
        type: LOGIN_USER_FAIL,
        error: ''
      });
      console.log('No default login');
    }
  });
};

export const loginUser = (loginType, email = '', pwd = '') => dispatch => {
  dispatch({ type: LOGIN_USER, loginType });
  _logInToFirebaseAndGetUserId(loginType, email, pwd)
    .then(userId => {
      firebase
        .database()
        .ref()
        .child('/users/')
        .child(userId)
        .once('value', snapshot => {
          if (snapshot.val() !== null) {
            dispatch({
              type: LOGIN_USER_SUCCESS,
              user: userId
            });

            Actions.main();
          } else {
            dispatch({ type: LOGIN_USER_FAIL, error: 'Not registered' });
          }
        });
    })
    .catch(error => {
      dispatch({ type: LOGIN_USER_FAIL, error: error.message });
    });
};

export const logoutUser = loginType => dispatch => {
  _logOut(loginType).then(
    () => {
      dispatch({ type: LOGOUT_USER });
      Actions.auth();
    },
    error => {
      console.log(error);
    }
  );
};

export const registerUser = (type, email = '', pwd = '') => dispatch => {
  dispatch({ type: REGISTER_USER, loginType: type });
  if (type === 'email') {
    firebase
      .auth()
      .createUserWithEmailAndPassword(email, pwd)
      .then(() => {
        _createUser(dispatch, type, email, pwd).then(() => {
          Actions.auth();
        });
      })
      .catch(error => {
        dispatch({ type: REGISTER_USER_FAIL, error: error.message });
      });
  } else {
    _createUser(dispatch, type, email, pwd);
  }
};

const _createUser = async (dispatch, type, email = '', pwd = '') => {
  _logInToFirebaseAndGetUserId(type, email, pwd)
    .then(userId => {
      const aidRef = firebase
        .database()
        .ref()
        .child('/users/');
      aidRef
        .child(userId)
        .set({
          likes: [],
          personal_informations: {
            about: '',
            alcohol: false,
            birthday: '1/1/1991',
            children: 0,
            eye_color: '',
            hair_color: '',
            height: 0,
            living_type: '',
            name: '',
            physique: '',
            relationship: '',
            sex: '',
            sexual_orientation: '',
            smoking: false,
            weight: 0,
            languages: []
          },
          pictures: []
        })
        .then(
          () => {
            const upRef = firebase
              .database()
              .ref()
              .child('/users_private/');
            upRef
              .child(userId)
              .set({
                account_settings: {
                  show_distance: true,
                  show_online_status: true
                }
              })
              .then(
                result => {
                  if (result) {
                    dispatch({ type: REGISTER_USER_SUCCESS });
                  } else {
                    dispatch({
                      type: REGISTER_USER_FAIL,
                      error: 'Already exists'
                    });
                  }
                },
                error => {
                  dispatch({ type: REGISTER_USER_FAIL, error: error.message });
                }
              )
              .catch(error => {
                dispatch({ type: REGISTER_USER_FAIL, error: error.message });
              });
          },
          error => {
            dispatch({ type: REGISTER_USER_FAIL, error: error.message });
          }
        )
        .catch(error => {
          dispatch({ type: REGISTER_USER_FAIL, error: error.message });
        });
    })
    .catch(error => {
      dispatch({ type: REGISTER_USER_FAIL, error: error.message });
    });
};

const _logInToFirebaseAndGetUserId = async (
  authType,
  email = '',
  password = ''
) => {
  let userInfo;
  switch (authType) {
    case 'facebook': {
      const accessToken = await _loginWithFacebook();
      userInfo = await firebase
        .auth()
        .signInWithCredential(
          firebase.auth.FacebookAuthProvider.credential(accessToken.accessToken)
        );
      break;
    }
    case 'google': {
      const userId = await _loginWithGoogle();
      userInfo = await firebase
        .auth()
        .signInWithCredential(
          firebase.auth.GoogleAuthProvider.credential(userId)
        );
      break;
    }
    case 'email': {
      userInfo = await firebase
        .auth()
        .signInWithEmailAndPassword(email, password);
      break;
    }
    default: {
      throw new Error('Invalid authentication type.');
    }
  }
  return userInfo.uid;
};

const _loginWithGoogle = async () => {
  try {
    await GoogleSignin.hasPlayServices({
      showPlayServicesUpdateDialog: true
    });
    const userInfo = await GoogleSignin.signIn();
    return userInfo.idToken;
  } catch (error) {
    if (error.code === statusCodes.SIGN_IN_CANCELLED) {
      throw new Error('User Cancelled the Login Flow');
    } else if (error.code === statusCodes.IN_PROGRESS) {
      throw new Error('Signing In');
    } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
      throw new Error('Play Services Not Available or Outdated');
    } else {
      throw new Error('Some Other Error Happened');
    }
  }
};

const _loginWithFacebook = async () => {
  try {
    await LoginManager.logInWithReadPermissions(['public_profile']);
    return await AccessToken.getCurrentAccessToken();
  } catch (error) {
    console.log('Error during facebook login: ', error);
  }
};

const _logOut = async loginType => {
  switch (loginType) {
    case 'facebook':
      await LoginManager.logOut();
      break;
    case 'google':
      await GoogleSignin.signOut();
      break;
    case 'email':
    default:
      break;
  }
  await firebase.auth().signOut();
};
