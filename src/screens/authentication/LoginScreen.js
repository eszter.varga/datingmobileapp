import React, { Component } from 'react';
import { Text, Dimensions } from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import { SocialIcon } from 'react-native-elements';
import { Card, CardSection } from '../../components/common';
import {
  loginUser,
  loginUserDefault,
  registerUser,
  clearError
} from '../../actions/auth';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loginType: ''
    };
  }

  componentWillMount() {
    this.props.clearError();
  }

  componentDidMount() {
    // TODO: this.props.loginUserDefault(null);
  }

  onLoginWithFacebookButtonPress = async () => {
    this.setState({ loginType: 'facebookLogin' });
    this.props.loginUser('facebook');
  };

  onLoginWithGoogleButtonPress = async () => {
    this.setState({ loginType: 'googleLogin' });
    this.props.loginUser('google');
  };

  onLoginWithEmailButtonPress = () => {
    Actions.loginwithemail();
  };

  onSignUpWithFacebookButtonPress = async () => {
    this.setState({ loginType: 'facebookSignUp' });
    this.props.registerUser('facebook');
  };

  onSignUpWithGoogleButtonPress = async () => {
    this.setState({ loginType: 'googleSignUp' });
    this.props.registerUser('google');
  };

  onSignUpWithEmailButtonPress = () => {
    Actions.signupwithemail();
  };

  renderButton = (callback, text, type, iconType) => {
    if (this.props.loading && this.state.loginType === type) {
      return (
        <SocialIcon
          button
          loading
          type={iconType}
          onPress={callback}
          style={{ width: Dimensions.get('window').width * 0.28 }}
        />
      );
    } else if (this.props.loading) {
      return (
        <SocialIcon
          button
          disabled
          type={iconType}
          onPress={callback}
          style={{ width: Dimensions.get('window').width * 0.28 }}
        />
      );
    }

    return (
      <SocialIcon
        button
        type={iconType}
        onPress={callback}
        style={{ width: Dimensions.get('window').width * 0.28 }}
      />
    );
  };

  render() {
    return (
      <Card>
        <CardSection>
          <Text>Log in</Text>
        </CardSection>
        <CardSection>
          {this.renderButton(
            this.onLoginWithFacebookButtonPress,
            'Log in with facebook',
            'facebookLogin',
            'facebook'
          )}
          {this.renderButton(
            this.onLoginWithGoogleButtonPress,
            'Log in with google',
            'googleLogin',
            'google-plus-official'
          )}
          {this.renderButton(
            this.onLoginWithEmailButtonPress,
            'Log in with Email',
            'emailLogin',
            'envelope'
          )}
        </CardSection>

        <CardSection>
          <Text>Don't have an account?</Text>
        </CardSection>
        <CardSection>
          <Text>Sign Up</Text>
        </CardSection>
        <CardSection>
          {this.renderButton(
            this.onSignUpWithFacebookButtonPress,
            'Sign up with facebook',
            'facebookSignUp',
            'facebook'
          )}
          {this.renderButton(
            this.onSignUpWithGoogleButtonPress,
            'Sign up with google',
            'googleSignUp',
            'google-plus-official'
          )}
          {this.renderButton(
            this.onSignUpWithEmailButtonPress,
            'Sign up with Email',
            'emailSignUp',
            'envelope'
          )}
        </CardSection>
      </Card>
    );
  }
}

const MapStateToProps = ({ auth }) => {
  const { loading } = auth;
  return { loading };
};

export default connect(
  MapStateToProps,
  { loginUser, loginUserDefault, registerUser, clearError }
)(LoginScreen);
