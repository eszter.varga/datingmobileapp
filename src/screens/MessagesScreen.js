import React, { Component } from 'react';
import {
  View,
  Text,
  TextInput,
  ScrollView,
  ListView,
  StyleSheet
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { chatRows, newMatches } from '../assets/data';
import ChatRow from '../components/ChatRow';
import RoundImageWithSubtitle from '../components/RoundImageWithSubtitle';

const ds = new ListView.DataSource({ rowHasChanged: (r1, r2) => r1 !== r2 });

class Messages extends Component {
  constructor(props) {
    super(props);

    this.state = {
      dataSource: ds.cloneWithRows(newMatches),
      chatRowData: ds.cloneWithRows(chatRows)
    };
  }

  onChatPartnerClick = () => {
    Actions.chat();
  };

  roundImageRender(RoundImageData, onPress) {
    return RoundImageWithSubtitle(
      RoundImageData.image,
      RoundImageData.first_name,
      onPress
    );
  }

  chatRowRender(chatRowItemData, onPress) {
    return ChatRow(
      chatRowItemData.image,
      chatRowItemData.name,
      chatRowItemData.message,
      onPress
    );
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <ScrollView style={styles.scrollViewContainer}>
          <TextInput style={styles.textInput} placeholder="Search" />
          <View style={styles.newMatchesContainer}>
            <Text style={styles.sectionTitleText}>NEW MATCHES</Text>
            <ListView
              horizontal
              showsHorizontalScrollIndicator={false}
              dataSource={this.state.dataSource}
              pageSize={5}
              renderRow={rowData =>
                this.roundImageRender(rowData, this.onChatPartnerClick)
              }
            />
          </View>
          <View style={styles.chatRowContainer}>
            <Text style={styles.sectionTitleText}>MESSAGES</Text>
            <ListView
              horizontal={false}
              scrollEnabled={false}
              showsHorizontalScrollIndicator={false}
              dataSource={this.state.chatRowData}
              pageSize={5}
              renderRow={rowData =>
                this.chatRowRender(rowData, this.onChatPartnerClick)
              }
            />
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1
  },
  scrollViewContainer: {
    flex: 1,
    padding: 10
  },
  newMatchesContainer: {
    borderTopWidth: 1,
    paddingTop: 15,
    borderTopColor: '#da533c',
    borderBottomWidth: 1,
    paddingBottom: 15,
    borderBottomColor: '#e3e3e3'
  },
  sectionTitleText: {
    color: '#da533c',
    fontWeight: '600',
    fontSize: 12
  },
  textInput: {
    height: 50
  },
  chatRowContainer: {
    margin: 10
  }
});

export default Messages;
