import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Button } from 'react-native-elements';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import { logoutUser } from '../../actions/auth';

class SettingsScreen extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.buttonContainer}>
          <Button
            title="PERSONAL INFORMATION"
            large
            color="black"
            backgroundColor="#f9f9f9"
            onPress={() => Actions.personalinformation()}
          />
        </View>

        <View style={styles.buttonContainer}>
          <Button
            title="ACCOUNT SETTINGS"
            large
            color="black"
            backgroundColor="#f9f9f9"
            onPress={() => Actions.accountsettings()}
          />
        </View>

        <View style={styles.buttonContainer}>
          <Button
            title="FAQ"
            large
            color="black"
            backgroundColor="#f9f9f9"
            onPress={() => Actions.faq()}
          />
        </View>

        <View style={styles.buttonContainer}>
          <Button
            title="LEGAL INFORMATION"
            large
            color="black"
            backgroundColor="#f9f9f9"
            onPress={() => {}}
          />
        </View>

        <View style={styles.buttonContainer}>
          <Button
            title="LOG OUT"
            large
            color="black"
            backgroundColor="#f9f9f9"
            onPress={() => {
              this.props.logoutUser(this.props.loginType);
            }}
          />
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  mainContainer: {
    flex: 1,
    justifyContent: 'space-around'
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'space-around'
  },
  button: {
    flex: 1,
    height: 300,
    backgroundColor: 'white'
  }
});

const MapStateToProps = () => ({});

export default connect(
  MapStateToProps,
  { logoutUser }
)(SettingsScreen);
