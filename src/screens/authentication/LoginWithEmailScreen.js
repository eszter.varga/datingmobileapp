import React, { Component } from 'react';
import { View, Text } from 'react-native';
import { connect } from 'react-redux';
import { loginUser, clearError } from '../../actions/auth';
import {
  Card,
  CardSection,
  Input,
  Button,
  Spinner
} from '../../components/common';

class LoginWithEmailScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      password: '',
      email: '',
    };
  }

  componentWillMount() {
    this.props.clearError();
  }

  onEmailChange = text => {
    this.setState({
      email: text
    });
  };

  onPasswordChange = text => {
    this.setState({
      password: text
    });
  };

  renderError = () => {
    if (this.props.error) {
      return (
        <View style={{ backgroundColor: 'white' }}>
          <Text style={styles.errorTextStyle}>{this.props.error}</Text>
        </View>
      );
    }
  };

  renderButton = () => {
    if (this.props.loading) {
      return <Spinner size="large" />;
    }

    return (<Button
      onPress={() => {
          this.props.loginUser(
            'email',
            this.state.email,
            this.state.password
          );
        }
      }
    >Login</Button>);
  };

  render() {
    return (
      <Card>
        <CardSection>
          <Input
            label="Email"
            placeholder="email@gmail.com"
            onChangeText={this.onEmailChange}
            value={this.state.email}
            keyboardType="email-address"
            textContentType="emailAddress"
          />
        </CardSection>
        <CardSection>
          <Input
            secureTextEntry
            label="Password"
            placeholder="password"
            onChangeText={this.onPasswordChange}
            value={this.state.password}
          />
        </CardSection>
        {this.renderError()}
        <CardSection>{this.renderButton()}</CardSection>
      </Card>
    );
  }
}

const styles = {
  errorTextStyle: {
    fontSize: 20,
    alignSelf: 'center',
    color: 'red'
  }
};

const MapStateToProps = ({ auth }) => {
  const { error, loading } = auth;
  return { error, loading };
};

export default connect(
  MapStateToProps,
  { loginUser, clearError }
)(LoginWithEmailScreen);
